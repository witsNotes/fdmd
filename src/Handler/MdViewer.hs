{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.MdViewer where
import MdToTree
import Data.List as L
import Import
import Yesod.Form.Bootstrap3
import Yesod.Markdown
import Data.Text (Text)
import System.IO as IO
import qualified Data.Text as T
import Data.Either
import Text.Pandoc.Options
import Data.Tree
import System.Directory
--import System.FilePath
-- import Shelly 
--import System.File.Tree

getMdViewerR :: Handler RepHtml
getMdViewerR = do
       file <- runInputGet $ ireq textField "file"
       directory <- runInputGet $ ireq textField "directory"
       let t = T.pack $ (T.unpack directory) L.++ (fileSeperator:(T.unpack file)) where fileSeperator ='/'
       let picFolder = (T.pack $L.reverse $ T.unpack $ T.append ( (T.pack (L.reverse ("Pics")))) (T.tail $ T.dropWhile (/= '.') $ T.reverse t)):: Text
       let tList = (T.splitOn (T.pack "/" ) picFolder):: [Text]
       let baseFolderName = L.head $L.reverse $ tList
       test <- liftIO (nonRecursiveCopyDirectory (T.unpack picFolder) ("static/" L.++ (T.unpack baseFolderName))) 
       fileContents <- (liftIO $ IO.readFile $T.unpack t)
       folder <-  (fileToAccordian fileContents)
       defaultLayout $ do
                $(widgetFile "md/new")

nonRecursiveCopyDirectory :: String -> String -> IO()
nonRecursiveCopyDirectory oldPath newPath = do
        isMissing <- doesDirectoryExist newPath
        a <- createDirectoryIfMissing isMissing newPath
        files <- listDirectory oldPath
        sequence_ $L.zipWith copyFile (L.map ((oldPath L.++"/") L.++) files) (L.map ((newPath L.++ "/") L.++)  files) 

foldForest' :: (a -> a -> Bool)-> [a] -> Forest a
foldForest' greaterThan [] = []
foldForest' greaterThan (x:[]) = [Node x []]
foldForest' greaterThan (x:lx) = (Node x (foldForest' greaterThan (L.takeWhile (greaterThan x) lx))): (foldForest' greaterThan $ L.dropWhile (greaterThan x) lx)

mdGreaterThan :: Markdown -> Markdown -> Bool
mdGreaterThan (Markdown s) (Markdown s') = myInt (T.unpack s) < myInt (T.unpack s') where
        myInt :: String -> Int
        myInt s = replaceZero $L.length $L.takeWhile (== '#') s where
                replaceZero :: Int -> Int
                replaceZero 0 = maxBound::Int
                replaceZero x = x 



myLayout :: Widget -> Handler Html
myLayout widget = do
        pc <- widgetToPageContent $ do
                widget
                toWidget [lucius| body { font-family: verdana } |]
        withUrlRenderer
                [hamlet|
                        $doctype 5
                        <html>
                          <head>
                           <title>#{pageTitle pc}
                           <meta charset=utf-8>
                              ^{pageHead pc}
                           <body>
                              <article>
                                ^{pageBody pc}
                |]


