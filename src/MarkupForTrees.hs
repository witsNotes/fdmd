module MarkupForTrees(
        MdForTree(..),
        HtmlHandlerForTree(..),
        stringToMdForTree,
        mdStringtoHtml,
        myReaderOptions,
        myWriterOptions,
        foldMdForest
        )where

-- This module contains everything short of the actual folding.


import Text.Pandoc.Options
import Yesod.Markdown
import qualified Data.Text as T
import Data.Tree
import Import (widgetToPageContent, Widget, Html, Handler, hamlet, lucius, withUrlRenderer,   newIdent)
import Merges
import Data.Either


foldForest :: (Ord a) => [a] -> Forest a
foldForest [] = []
foldForest (x:[]) = [Node x []]
foldForest (x:lx) = (Node x (foldForest (takeWhile (< x) lx))): (foldForest $ dropWhile ( < x) lx)

data MdForTree =
    Heading String 
    | Indent (Forest MdForTree) 
        --This is the lowest option in a tree but contains an entire tree under it.
    | Line String deriving (Show)

data HtmlHandlerForTree =  
    HtmlHeading (Handler Html) 
    | HtmlIndent (Forest (HtmlHandlerForTree) )
        --This is the lowest option in a tree but contains an entire tree under it.
    | HtmlLine (Handler Html)

instance Eq MdForTree where
        (==) (Heading s) (Heading s') = (length $ takeWhile (== '#') s) == (length $ takeWhile (== '#') s')
        (==) (Heading _) _ = False
        (==) _ (Heading _) = False
        (==) _ _ = True

instance Ord MdForTree where
        (<=) (Heading s) (Heading s') =  (length $ takeWhile (== '#') s')<= (length $ takeWhile (== '#') s)
        (<=) (Heading _) _ = False
        (<=) _ (Heading _) = True
        (<=) _ _ = True

stringToMdForTree :: String -> MdForTree
stringToMdForTree s
        |isHeading s = Heading s
        |indent s = Indent (foldMdForest $ unlines $ map indentTail$ lines s)
        -- this is temporary
        |otherwise = Line s

indentTail::String -> String
indentTail []  = []
indentTail x = tail $ dropWhile (== ' ') x

mdStringtoHtml :: String -> Html
mdStringtoHtml s = (fromRight (error "failed to parse markdown") .( markdownToHtmlWith myReaderOptions myWriterOptions) $ toMd s) where
        toMd :: String -> Markdown
        toMd s = Markdown $ T.pack s

myReaderOptions :: ReaderOptions
myReaderOptions = def{
  readerExtensions = pandocExtensions
}

myWriterOptions :: WriterOptions
myWriterOptions = def{
   writerHTMLMathMethod = MathJax "https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
  }

foldMdForest :: String -> Forest (MdForTree )
foldMdForest s =  foldForest $ map (stringToMdForTree) (mergeLines $map rIW $ mergeIndent $ mergeSource $ lines $ s) 

rIW :: String -> String
rIW s = indents ++ renameImg(afterIndent) where 
        afterIndent = dropWhile(\x -> (x== '>' || x == ' ')) s
        indents = takeWhile (\x -> (x =='>' || x == ' ')) s

renameImg :: String -> String
renameImg s
        |take 2 s == "![" = (takeWhile (/= '(') s) ++ "(/static/" ++ (tail (dropWhile (/='(') s))
        |otherwise = s 

