{-# LANGUAGE PackageImports #-}
import "fdmd" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
